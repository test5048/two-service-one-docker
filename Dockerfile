FROM golang:1.13-alpine

WORKDIR /app

COPY *.go go.mod start.sh ./

RUN go mod download && chmod +x start.sh
RUN go build -o ./add addition.go && go build -o ./mult multiplication.go

EXPOSE 8080 8081

CMD ["./start.sh"]