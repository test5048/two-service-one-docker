package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
)

func main() {
	http.HandleFunc("/multiplication", MultiplicationServer)
	
	fmt.Printf("Starting server at port 8080\n")
	// Starts server on port 8080, log Fatal error if failure
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func MultiplicationServer(w http.ResponseWriter, r *http.Request) {
	// Fetch all values under key "term"
	query := r.URL.Query()["term"]
	// Start with default answer 1 and multiply terms on top of it
	ans := 1

	// Loop through all the terms in request
	for i := 0; i < len(query); i++ {

		// Convert query value to int, if successful multiply to existing answer otherwise do nothing
		if term, err := strconv.Atoi(query[i]); err == nil {
			ans *= term
		}
	}

	// Catch any unforeseen nasty formatting errors as Fatal
	_, err := fmt.Fprintf(w, "%v\n", ans)
	if err != nil {
		log.Fatal(err)
	}
}
