package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
)

func main() {
	http.HandleFunc("/addition", AdditionServer)
	
	fmt.Printf("Starting server at port 8081\n")
	// Starts server on port 8081, log Fatal error if failure
	log.Fatal(http.ListenAndServe(":8081", nil))
}

func AdditionServer(w http.ResponseWriter, r *http.Request) {
	// Fetch all values under key "addend"
	query := r.URL.Query()["addend"]
	// Start with default answer 0 and add addends on top of it
	ans := 0

	// Loop through all the addends in request
	for i := 0; i < len(query); i++ {

		// Convert query value to int, if successful add to existing answer otherwise do nothing
		if addend, err := strconv.Atoi(query[i]); err == nil {
			ans += addend
		}
	}

	// Catch any unforeseen nasty formatting errors as Fatal
	_, err := fmt.Fprintf(w, "%v\n", ans)
	if err != nil {
		log.Fatal(err)
	}
}
